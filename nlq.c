/*
 * Copyright (C) 2007 Alejandro Mery <amery@geeks.cl>
 *
 * More information can be found in the files COPYING and README.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3 of the License. A copy of the
 * GNU General Public License can be found in the file COPYING.
 */

#include "nlq.h"

#include <stdlib.h>

int main( int argc, char **argv ) {
	link_t l;
	if ( nl_open( &l ) < 0 )
		return EXIT_FAILURE;
	nl_close( &l );	
	return EXIT_SUCCESS;
	return EXIT_SUCCESS;
}
