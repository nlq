
/*
 * Copyright (C) 2007 Alejandro Mery <amery@geeks.cl>
 *
 * More information can be found in the files COPYING and README.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3 of the License. A copy of the
 * GNU General Public License can be found in the file COPYING.
 */

#ifndef __NETLINK_H__
#define __NETLINK_H__

#include <sys/socket.h>	// socket()
#include <linux/netlink.h>	// struct sockaddr_nl

typedef struct {
	int			fd;
	struct sockaddr_nl	sa;
	} link_t;

int nl_open( link_t *self );
inline int nl_close( link_t *self );

#endif

