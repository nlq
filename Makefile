CC=diet gcc
CFLAGS=-Os -Wall

TOOL=nlq
OBJECTS=nlq.o netlink.o

.PHONY: all clear test

all: $(TOOL)

$(TOOL): $(OBJECTS)

clean:
	rm -f *.o $(TOOL)

test: all
	./$(TOOL) || echo $$?
