/*
 * Copyright (C) 2007 Alejandro Mery <amery@geeks.cl>
 *
 * More information can be found in the files COPYING and README.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3 of the License. A copy of the
 * GNU General Public License can be found in the file COPYING.
 */

#include "netlink.h"

#include <unistd.h>	// close()
#include <stdio.h>	// perror()

int nl_open( link_t *self ) {
	// initialize
	memset(self, 0x00, sizeof(self));

	// open the socket
	if ( (self->fd = socket( AF_NETLINK, SOCK_RAW, NETLINK_ROUTE )) < 0 ) {
		perror("socket");
		return -1;
	}

	// bind it to the address
	self->sa.nl_family = AF_NETLINK;
	if ( bind( self->fd, (const struct sockaddr*) &self->sa, sizeof( self->sa ) ) < 0 ) {
		perror("bind");
		close( self->fd );
	}

	return self->fd;
}

inline int nl_close( link_t *self ) {
	return close( self->fd );
}
